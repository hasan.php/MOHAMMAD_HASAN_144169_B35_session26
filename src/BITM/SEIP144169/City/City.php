<?php
namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;




class City extends DB
{
    public $id;
    public $name;
    public $city;


    public function __construct()
    {
        parent::__construct();
        if(!isset( $_SESSION)) session_start();
    }

    public function index()
    {
        echo "Im inside index method of City!";
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('city', $postVariableData)) {
            $this->city = $postVariableData['city'];
        }

    }//end of setData method

    public function store()
    {
        $arrData = array($this->name, $this->city);
        $sql = "Insert INTO city(name,city) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }
}
